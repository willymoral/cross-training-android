package com.rga.skillsearch.skillsearch.api;

import com.rga.skillsearch.skillsearch.request.UpdateSkillRequest;
import com.rga.skillsearch.skillsearch.response.MySkillResponse;
import com.rga.skillsearch.skillsearch.response.SkillResponse;
import java.util.Map;
import retrofit.Callback;
import retrofit.client.Response;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Query;

/**
 * Created by guillermomo on 7/2/15.
 */

public interface SkillsApi
{
    /**
     * Get All Skills
     *
     * @param value
     * @param callback
     */
    @GET("/api/skill")
    void getAllSkill(@Query(value = "ipp", encodeValue = false) String value, Callback<SkillResponse> callback);

    /**
     * Update Skill
     *
     * @param updateSkill
     * @param callback
     */
    @POST("/api/user-skill")
    void updateSkill(@Body UpdateSkillRequest updateSkill, Callback<Response>callback);

    /**
     * Search people with Skill
     *
     * @param options
     * @param callback
     */
    @GET("/api/user")
    void getUsersWithSkill(@Query("options") Map<String, String> options , Callback<MySkillResponse> callback);
}