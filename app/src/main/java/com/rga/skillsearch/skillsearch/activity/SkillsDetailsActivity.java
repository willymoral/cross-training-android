package com.rga.skillsearch.skillsearch.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import com.rga.skillsearch.skillsearch.adapter.CategoryAdapter;
import com.rga.skillsearch.skillsearch.R;
import com.rga.skillsearch.skillsearch.db.DataManager;
import com.rga.skillsearch.skillsearch.response.SkillResponse;

import java.util.ArrayList;

public class SkillsDetailsActivity extends AppCompatActivity
{
    private RecyclerView recyclerView;
    private CategoryAdapter adapter;
    private ArrayList itemsData = new ArrayList();
    private Context context;
    private ProgressDialog progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_skills_details);
        SkillResponse.Category category = (SkillResponse.Category) getIntent().getSerializableExtra("Category");

        Log.d("Category ID: ", category.getIdCategory());

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        // Retrive Data
        DataManager dataManager = DataManager.getInstance();
        this.itemsData = dataManager.getCategorys();

        adapter = new CategoryAdapter(itemsData);
        recyclerView.setAdapter(adapter);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
    }
}
