package com.rga.skillsearch.skillsearch.response;

import android.util.Log;

import com.google.gson.annotations.SerializedName;
import com.rga.skillsearch.skillsearch.model.SkillData;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by guillermomo on 7/17/15.
 */
public class MySkillResponse
{
    public Pager pager;
    @SerializedName("data")
    public List<Data> dataSkill;

    public ArrayList<Data>getData()
    {
        ArrayList items = new ArrayList();
        int dataCount = this.dataSkill.size();

        for(int i=0; i < dataCount ; i++)
        {
            items.add(this.dataSkill.get(i));
        }

        return items;
    }

    public class Pager
    {
        public String p;
        public String total_pages;
        public String ipp;
        public String page_count;
        public String total_count;
        public String filters;
        // Research Sort
        public String prev;
        public String current;
        public String next;
    }

    public class Data
    {
        @SerializedName("_id")
        public String idData;
        @SerializedName("_version")
        public String versionData;
        public String departmanet;
        public String email;
        public String firstname;
        public String lastname;
        public String office;
        @SerializedName("title")
        public String titleData;
        public String username;
        public String updated;
        @SerializedName("created")
        public String createdData;
        public List<Skills>skills;
        public List<SkillLevels>skill_levels;

        public List<Skills> getSkills()
        {
            return skills;
        }
        public List<SkillLevels> getSkillLevels()
        {
            return skill_levels;
        }
    }

    public class Skills
    {
        @SerializedName("_id")
        public String idSkill;
        @SerializedName("_version")
        public String versionSkills;
        public String category;
        @SerializedName("title")
        public String titleSkill;
        public String updated;
        @SerializedName("created")
        public String createdSkill;
        public String order;
        public String userCreated;
    }

    private class SkillLevels
    {
        @SerializedName("_id")
        public String idSkill;
        @SerializedName("_version")
        public String versionSkills;
        public String level;
        public String skill;
        public String user;
        public String updated;
        @SerializedName("created")
        public String createdSkill;
        public String location;
        public String want;
    }

    public ArrayList<SkillData> getItem()
    {
        ArrayList items = new ArrayList();

        int skillCount = this.dataSkill.get(0).skills.size();
        for(int i=0; i < skillCount ; i++)
        {
            Skills item =  this.dataSkill.get(0).skills.get(i);

            int skillLevelCount = this.dataSkill.get(0).skill_levels.size();

            String skillLevel = "0";

            for(int j=0; j < skillLevelCount; j++)
            {
                SkillLevels level = this.dataSkill.get(0).skill_levels.get(j);

                if(item.idSkill.equalsIgnoreCase(level.skill))
                {
                    skillLevel = level.level;
                    break;
                }
            }

            SkillData tmp = new SkillData(item.idSkill, item.titleSkill, skillLevel);
            items.add(tmp);
        }

        return items;
    }
}
