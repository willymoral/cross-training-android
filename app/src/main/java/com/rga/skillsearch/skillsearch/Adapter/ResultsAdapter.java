package com.rga.skillsearch.skillsearch.adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;

import com.rga.skillsearch.skillsearch.R;
import com.rga.skillsearch.skillsearch.response.MySkillResponse;
import com.rga.skillsearch.skillsearch.response.SkillResponse;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by guillermomo on 10/14/15.
 */
public class ResultsAdapter extends RecyclerView.Adapter<ResultsAdapter.ViewHolder>
{
    private List<MySkillResponse.Data> itemsData = Collections.emptyList();
    private Context context;
    private ProgressDialog progress;
    AdapterView.OnItemClickListener itemClickListener;

    public ResultsAdapter(Context context, ArrayList itemsData)
    {
        this.itemsData = itemsData;
        this.context = context;
        this.progress = new ProgressDialog(this.context);
    }

    public ResultsAdapter(ArrayList itemsData)
    {
        this.itemsData = itemsData;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ResultsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                                 int viewType)
    {
        // Create a new view
        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.skill_item, null);

        // Create ViewHolder
        ViewHolder viewHolder = new ViewHolder(itemLayoutView);
        return viewHolder;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position)
    {
        MySkillResponse.Data tmp = itemsData.get(position);
        viewHolder.categoryTextView.setText(tmp.firstname + " " + tmp.lastname);
    }

    // inner class to hold a reference to each item of RecyclerView
    public class ViewHolder extends RecyclerView.ViewHolder
    {
        public TextView categoryTextView;

        public ViewHolder(View v)
        {
            super(v);
            categoryTextView = (TextView) v.findViewById(R.id.skillTextView);

            v.setOnClickListener(
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            // TODO
                        }
                    });
        }
    }

    // Return the size of your itemsData (invoked by the layout manager)
    @Override
    public int getItemCount()
    {
        return itemsData.size();
    }

    public void setItemsData(ArrayList itemsData)
    {
        this.itemsData.clear();
        this.itemsData.addAll(itemsData);
    }
}