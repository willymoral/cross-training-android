package com.rga.skillsearch.skillsearch.activity;

import android.app.Activity;
import android.os.Bundle;
import android.widget.SearchView;
import com.rga.skillsearch.skillsearch.R;
import com.rga.skillsearch.skillsearch.db.DataManager;
import com.rga.skillsearch.skillsearch.response.LoginResponse;

public class SearchActivity extends Activity
{
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        DataManager dataManager = DataManager.getInstance();
        LoginResponse loginResponse = dataManager.getLoginResponse();

        SearchView searchView = (SearchView) findViewById(R.id.searchView);
        searchView.setIconifiedByDefault(false);

    }
}
