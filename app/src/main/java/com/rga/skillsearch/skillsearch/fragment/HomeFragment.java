package com.rga.skillsearch.skillsearch.fragment;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.rga.skillsearch.skillsearch.R;
import com.rga.skillsearch.skillsearch.activity.SearchActivity;
import com.rga.skillsearch.skillsearch.api.ServiceManager;
import com.rga.skillsearch.skillsearch.db.DataManager;
import com.rga.skillsearch.skillsearch.response.LoginResponse;
import com.rga.skillsearch.skillsearch.response.SkillResponse;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Set;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by guillermomo on 10/8/15.
 */
// Home
public class HomeFragment extends Fragment
{
    private View rootView;
    private Context context;
    private ProgressDialog progress;

    public HomeFragment() {}

    public HomeFragment(Context context)
    {
        this.context = context;
        this.progress = new ProgressDialog(this.context);

        final DataManager dataManager = DataManager.getInstance();

        if (!dataManager.isAllSkill())
        {
            this.progress.setMessage("Loading...");
            this.progress.show();

            // Check Get all category
            ServiceManager serviceManager = ServiceManager.getInstance();
            String ipp = "1000";

            serviceManager.getSkillApi().getAllSkill(ipp, new Callback<SkillResponse>() {
                @Override
                public void success(SkillResponse skillResponse, Response response) {
                    progress.dismiss();
                    Set categorys = skillResponse.getCategory();
                    dataManager.setAllSkillResponse(skillResponse);
                    dataManager.setCategorys(categorys);



                    Log.w("myApp", "All Skill - OK!");
                }

                @Override
                public void failure(RetrofitError error) {
                    progress.dismiss();
                    Log.w("myApp", "All Skill - ERROR!");
                }
            });
        }
    }

    public void setContext(Context context)
    {
        this.context = context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        this.rootView = inflater.inflate(R.layout.fragment_main_screen, container, false);

        DataManager dataManager = DataManager.getInstance();
        LoginResponse loginResponse = dataManager.getLoginResponse();

        TextView firstName = (TextView) rootView.findViewById(R.id.firstNameTextView);
        TextView lastName = (TextView) rootView.findViewById(R.id.lastNameTextView);
        TextView roleTextView = (TextView) rootView.findViewById(R.id.roleTextView);
        TextView emailTextView = (TextView) rootView.findViewById(R.id.emailTextView);
        TextView skillTextView = (TextView) rootView.findViewById(R.id.skillTextView);
        ImageView avatarImageView = (ImageView) rootView.findViewById(R.id.avatarImageView);
        Button searchButton = (Button)rootView.findViewById(R.id.searchButton);

        firstName.setText(loginResponse.getUser().firstname);
        lastName.setText(loginResponse.getUser().lastname);
        emailTextView.setText(loginResponse.getUser().email);
        roleTextView.setText(loginResponse.getUser().title);

        skillTextView.setText(getString(R.string.total_skill) + loginResponse.getUser().getSkills().size());

        String encodedImage = loginResponse.getUser().thumb;

        //encodedImage = "/9j/4AAQSkZJRgABAQEAYABgAAD/2wBDAAgGBgcGBQgHBwcJCQgKDBQNDAsLDBkSEw8UHRofHh0aHBwgJC4nICIsIxwcKDcpLDAxNDQ0Hyc5PTgyPC4zNDL/2wBDAQkJCQwLDBgNDRgyIRwhMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjL/wAARCACAAIADASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwDtvy/IUvPoPyFKFFOC1yWNxBn0H5CnD6D8hShaeBVJAMAPoPyFLg+g/IU/FAGT9KdguNAPt+Qo59vyFOYhFLsQEXqew/GsO58XaRZztDcXAidegb+L6YosK5tEn2/IUmT7fkKihvrS5jR47mIq/wB1t3WrGAw+XBH95TkU7BcjOT2H5Ck5HYfkKkxgA44PQ0YosFyPPsP++RRz6D8hT8e1BFKwXIjken5Ckyfb8hUhGabiiwCgU8CkAp4FIYAYpwoxxSgVSEGMDJrmvE3jLTvDamKYma6YfLCvr7ntWxrGojStIur4jf5UZO33HSvnbVNQuNTv5rydy0srkn29qA06mv4j8b6tr8zo832e35AghOBj39a5nzHKkSMSOgGaPLaR8KrMx9qnh027nlKCBsgdTVWQkmxiaje2sBijuH8o9QGNbeieNNZ06eFlvZZII2yY2bgjuKxY7SaVjGkRJHU4qJoGgd1YbRjJz0NOyFZo+ltK1S21jTor+1cPDKox8uCpHUVd7V5R8KNYlju7rSpCWidPOi3P9w9wo969X79KLAJijFLRipGNxTSKkIppFAxBT+9NAp1QMWlFGKUCmBxfxNumt/ChVW2+bMoYex614/p+nXOpXCpBFkdGPvXsHxOtZLnwqSigrE4dz34rl/h+kMWgvcyMilpSrM38OKmpJxV0aUoc0rMj0Hww9tPI13Fz0Uit+70GG5j8pGMSn7+zvV6PXdJLBftiBucA1bilgnG+OVSv+yaxvN6nbGnAz7bQbOFBHFEo45IrI1jwWlzbSNAMOoLLW7d61aaaPmDu2f4V/SmWHiO5vZQz6b5Vtnht+Wx9K2g5GFWMeh5Rossula7azqSDDOAB75wa+iFOV5rw3UNKmTx3JZiPc73IlVUPGGOa9xGNo7ZA/CttzjejHUUUVIAaaadTSKAAGnCminCoKHAZpelJSg4qkwKGr6ZFq+mT2cuSJFIzXmnh/wAPz6O+qadcgP8AMjDByCOeK9azjOOOawdQgSLUGbvIASfpWNa/LobULOZ5xqOl6k1wjoLcRg427fu10Hh6C4jz5/KH0GK1rjyY23E8npgVDHdxBtsjqp/unisnKVj0VTjYztc0WW8udyO4hGDsU4Bq1pWjwW7q671kDF1VeFXPUYrTlnV4wqNu+WmWFyrTBSMHpzW6djHlXLdj7HSIj4tutQeMeYsaBG9OOa6QfwnuOvvUEW0u7gctjNTA1vHY82o7yH5opuaTNJkofTSaTdikL0hig07NRg04GoRQ/NGabmgGqQDs1ma0CIYpB0U81og1Xu1WW1kVyAoGST296Jq6CEuWVzkL3eB5i5IHOB1NZUl7FJ9+C4GequuKvpf211K0cUqu0YDEjowPQ0+aKKVQ2QGPcjmuTZ2PThK5Ttr7kxR2crY+6S2AfatC1julvUe4hELHoiNnb7E1JpwEBJbBwOAB0q5OxdOP9ZtLDJ5OBmt1qjGpKzNm2J2Ek9+cdKsbuK5DT/GFlDZ2p1E/ZzNnLkfIp9CfWumguobmJZIZEkUjIKtnNdKWh50nqWQ1OzUAfnr+fSnBs0mhpku6mlqTIppNS0Fx4NLmog1BasyyXdjrSF/TJPpURc/571h+IPFOn+H4c3Um+VvuQqeaqKC5uS3McMbSSyLGijJZjgCvLfGvjyLULaXTdMLeWG2yTk4DD0A7g1zXiLxjqPiGQxy/u7RTvSBTgexPqa5wPl+SCQDge9bKGhk27nqPh6NJ0DSKgNzbo6BBgLt61bunurCXbInnRDncOoFcZ4c8SLBGtneTOmxh9nlI+VfVW9q9AF1FdQp86liMkZyv4VwzpyUtDvp1YyRQs9UkuJWSKKQMeMleK6e3j+y2Mk87AyGJgSecccVmWirE2cqB61V1jW7cwPHLcLHaL/rDnl/YVrG9rWM5vW9zn/ECxr4JQSrskaTcoz0auLsdbvtNybO6lhViMhW+9j1q34k17+27hViBS3j4VPX3rCJPAxwK7KS93U46msrnoOnfFK9gBF/aR3IOACvy7a7jR/Gmk63L5UU7Rznok3Bb2FeCnjipYpmhdWViGU5DL1FW4pkXsfSwb1yD6GlzXmHhPx3IjrY6q7PuIEco65PYmvSQQeQQcnjd6VhJWNIu4/fQXGRn8OOpqDdxVPUdTh0vT5ry4fbHGv5nsBXOt7GzMfxj4wj8PWv2eE79QkXKAjhR/erxi7u5ru4ea4laSZzli55J9Ksarqs+rajJe3Ds0jNkZ6AdgPQVnMcs36e1dUIaGDkLnNBJI20zNKD+daIkRuQQRle9WI7u6i2+Xcyp124bpUGOPalyOeOtJxTGny7F0a1qO4Zv5j/wKq8k0s8hklkZ29WOaiBxj2pfU5600khNth8wJ96M+ooJzgUMOeDxVXEAO0ZpASF+tIeRihm+VaALMT/KuCQc8Edc17Z4Q1j+1tBhlZszQ/u5B6n1NeGRvhcDuevpXY/D7VfsWvJaMx2XQKHJwN3Y0pK6JjdM9c3epwPWuD+JuoeVpNrZlNwnk3ls427a7bJJxlck4UFhXkfxFvluvEXkxTebFDGFI/ut3rhpq7OyWxybcbhnBH/j1RqwK5z0pC2eQdw7mmqeSMV2o5iUc0DBppOKO3vQA/ODg0LnPamdvel3YoAduFHHrTQM0bwOMimhEgOOeDSEgjJqPdjuKM5OKYDzSN/qgffFL2pjNiIj/aFIByYAwe/Sporg210k6klkKkY4xg5qqHA2qfWh2+Y4ouOx/9k=";

        if (encodedImage != null)
        {
            byte[] decodedString = Base64.decode(encodedImage, Base64.NO_WRAP);
            InputStream inputStream  = new ByteArrayInputStream(decodedString);
            Bitmap bitmap  = BitmapFactory.decodeStream(inputStream);
            avatarImageView.setImageBitmap(bitmap);
        } else
        {
            avatarImageView.setImageResource(R.drawable.no_photo);
        }

        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(context, SearchActivity.class);
                startActivity(intent);
            }
        });

        return rootView;
    }
}
