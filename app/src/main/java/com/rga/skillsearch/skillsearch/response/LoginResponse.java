package com.rga.skillsearch.skillsearch.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by guillermomo on 6/26/15.
 */

public class LoginResponse
{

    public User user;
    public String role;

    public class User
    {
        @SerializedName("_id")
        public String userId;
        @SerializedName("_version")
        public String userVersion;
        public String department;
        public String email;
        public String firstname;
        public String lastname;
        public String office;
        public String title;
        public String username;
        public String updated;
        public String created;
        public List<String>skills;
        @SerializedName("skill_levels")
        public List<SkillLevels>skillLevels;
        public String role;
        public String thumb;

        public List<String> getSkills()
        {
            return skills;
        }

        public List<SkillLevels> getSkillLevels()
        {
            return skillLevels;
        }
    }

    public class SkillLevels
    {
        @SerializedName("_id")
        public String idLevel;
        @SerializedName("_version")
        public String skilllevelsVersion;
        public String level;
        @SerializedName("skill")
        public String idSkill;
        public String user;
        public String updated;
        public String created;
        public String location;
        public String want;
    }

    public User getUser()
    {
        return user;
    }

    public void setUser(User user)
    {
        this.user = user;
    }

    public String getRole()
    {
        return role;
    }

    public void setRole(String role)
    {
        this.role = role;
    }

    public LoginResponse(User user, String role)
    {
        this.user = user;
        this.role = role;
    }
}
