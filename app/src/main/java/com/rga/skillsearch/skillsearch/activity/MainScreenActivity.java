package com.rga.skillsearch.skillsearch.activity;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import com.rga.skillsearch.skillsearch.R;
import com.rga.skillsearch.skillsearch.db.DataManager;
import com.rga.skillsearch.skillsearch.fragment.*;

public class MainScreenActivity extends Activity
{
    private Toolbar toolbar;
    private Button save;
    private DrawerLayout drawerLayout;
    private ListView drawerList;
    private Fragment fragment = null;
    private ProgressDialog progressDialog;

    private CharSequence drawerTitle;
    private CharSequence title;
    private String[] options;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_screen);
        progressDialog = new ProgressDialog(this);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        save = (Button) findViewById(R.id.saveButton);

        if (toolbar != null)
        {
            toolbar.setTitle(getString(R.string.toolbar_title));
            toolbar.setTitleTextColor(Color.WHITE);
            toolbar.setNavigationIcon(R.drawable.ic_drawer);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v)
                {
                    drawerLayout.openDrawer(drawerList);
                }
            });
        }

        title = drawerTitle = getTitle();

        options = getResources().getStringArray(R.array.main_screen_array);

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        drawerList = (ListView) findViewById(R.id.left_drawer);

        // Set up the drawer's list view with items and click listener
        drawerList.setAdapter(new ArrayAdapter<String>(this,
                R.layout.drawer_list_item, options));

        drawerList.setOnItemClickListener(new DrawerItemClickListener());

        // Show My Skill by default.
        this.selectItem(0);
    }

    private class DrawerItemClickListener implements ListView.OnItemClickListener
    {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            selectItem(position);
        }
    }

    private void selectItem(int position)
    {
        save.setVisibility(View.INVISIBLE);

        switch (position)
        {
            case 0:
                // Home
                toolbar.setTitle(getString(R.string.toolbar_title));
                fragment = new HomeFragment(MainScreenActivity.this);
                break;

            case 1:
                // My Skills
                /*
                fragment = new MySkillFragment(MainScreenActivity.this);

                toolbar.setTitle(getString(R.string.my_skill_option));
                save.setVisibility(View.VISIBLE);
                save.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        MySkillFragment tmpFragment = (MySkillFragment)fragment;
                        MySkillAdapter tmpAdapter = (MySkillAdapter)tmpFragment.adapter;
                        tmpAdapter.saveSkills();
                    }
                });
                */

                // My Skills
                toolbar.setTitle("My Skill");
                fragment = new MySkillCategoryFragment(MainScreenActivity.this);
                break;

            case 2:
                // All Skills
                toolbar.setTitle(getString(R.string.all_skill_option));
                fragment = new AllSkillFragment(MainScreenActivity.this);
                break;

            case 3:
                // Platform
                toolbar.setTitle(getString(R.string.platform_option));
                fragment = new PlatformFragment();
                break;

            case 4:
                DataManager dataManager = DataManager.getInstance();
                dataManager.setLoginResponse(null);
                break;
        }

        if (position!=4)
        {
            // Update Fragment
            FragmentManager fragmentManager = getFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();

            // Update selected item and title, then close the drawer
            drawerList.setItemChecked(position, true);
            setTitle(options[position]);
            drawerLayout.closeDrawer(drawerList);
        } else
        {
            finish();
        }
    }

    @Override
    public void setTitle(CharSequence title)
    {
        this.title = title;
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState)
    {
        super.onPostCreate(savedInstanceState);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig)
    {
        super.onConfigurationChanged(newConfig);
    }
}