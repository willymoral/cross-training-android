package com.rga.skillsearch.skillsearch.adapter;

/**
 * Created by guillermomo on 7/17/15.
 */
public interface MySkillsAdapterCallback
{
    void showProgressBar(String title, String message);
    void dismissProgressBar();
    void showError(String Error);
}

