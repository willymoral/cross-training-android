package com.rga.skillsearch.skillsearch.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.rga.skillsearch.skillsearch.R;
import com.rga.skillsearch.skillsearch.adapter.ResultsAdapter;
import com.rga.skillsearch.skillsearch.api.ServiceManager;
import com.rga.skillsearch.skillsearch.response.MySkillResponse;
import com.rga.skillsearch.skillsearch.response.SkillResponse;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class ResultsSkillActivity extends AppCompatActivity
{
    private RecyclerView recyclerView;
    private ResultsAdapter adapter;
    private ArrayList itemsData = new ArrayList();
    private Context context;
    private ProgressDialog progress;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_results_skill);
        SkillResponse.Data skillData = (SkillResponse.Data) getIntent().getSerializableExtra("Skill");

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        // Retrive data from service
        ServiceManager serviceManager = ServiceManager.getInstance();
        Map mapFilter = new HashMap();
        String filter = "skills=$in[" + skillData.idSkill + "]";

        mapFilter.put("filters[]=",filter);
        mapFilter.put("ipp=", "999");

        serviceManager.getSkillApi().getUsersWithSkill(mapFilter, new Callback<MySkillResponse>() {
            @Override
            public void success(MySkillResponse mySkillResponse, Response response)
            {
                itemsData = mySkillResponse.getData();

                adapter = new ResultsAdapter(itemsData);
                recyclerView.setAdapter(adapter);
                recyclerView.setItemAnimator(new DefaultItemAnimator());
            }

            @Override
            public void failure(RetrofitError error)
            {
                Log.w("myApp", "ResultsSkillActivity - Error!");
            }
        });
    }
}