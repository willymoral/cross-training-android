package com.rga.skillsearch.skillsearch.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.rga.skillsearch.skillsearch.R;

/**
 * Created by guillermomo on 10/8/15.
 */

public class PlatformFragment extends Fragment
{
    public PlatformFragment()
    {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        final View rootView = inflater.inflate(R.layout.fragment_all_skill, container, false);

        return rootView;
    }
}