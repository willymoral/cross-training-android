package com.rga.skillsearch.skillsearch.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import com.rga.skillsearch.skillsearch.adapter.CustomAdapter;
import com.rga.skillsearch.skillsearch.model.ItemData;
import com.rga.skillsearch.skillsearch.R;

public class ListActivity extends AppCompatActivity
{
    private static final String TAG = "RecyclerView";

    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);

        ItemData itemsData[] = { new ItemData("My Skills","Bla Bla Bla"),
                new ItemData("All Skills","Ble Ble Ble"),
                new ItemData("Platform","Bli Bli Bli"),
                new ItemData("Logout","Blu Blu Blu"),
                new ItemData("Test 1","Blu Blu Blu"),
                new ItemData("Test 2","Blu Blu Blu"),
                new ItemData("Test 3","Blu Blu Blu"),
                new ItemData("Test 4","Blu Blu Blu"),
                new ItemData("Test 5","Blu Blu Blu"),
                new ItemData("Test 6","Blu Blu Blu"),
                new ItemData("Test 7","Blu Blu Blu"),
                new ItemData("Test 8","Blu Blu Blu"),
                new ItemData("Test 9","Blu Blu Blu"),
                new ItemData("Test 10","Blu Blu Blu"),
                new ItemData("Test 11","Blu Blu Blu")};

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new CustomAdapter(itemsData);
        recyclerView.setAdapter(adapter);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
    }

    @Override
    protected void onStart()
    {
        super.onStart();
    }
}
