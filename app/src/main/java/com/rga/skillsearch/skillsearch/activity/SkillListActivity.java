package com.rga.skillsearch.skillsearch.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import com.rga.skillsearch.skillsearch.R;
import com.rga.skillsearch.skillsearch.adapter.SkillListAdapter;
import com.rga.skillsearch.skillsearch.model.ItemData;


public class SkillListActivity extends AppCompatActivity
{
    private static final String TAG = "RecyclerView";

    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_skill_list);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);

        // TODO get itemData
        ItemData itemsData[] = {new ItemData(getString(R.string.home_skill_list),""),
                new ItemData(getString(R.string.my_skills_list),""),
                new ItemData(getString(R.string.all_skills_list),""),
                new ItemData(getString(R.string.platform_list),""),
                new ItemData(getString(R.string.logout_list),"")};

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new SkillListAdapter(itemsData);
        recyclerView.setAdapter(adapter);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
    }

    @Override
    protected void onStart()
    {
        super.onStart();
    }
}
