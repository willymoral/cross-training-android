package com.rga.skillsearch.skillsearch.model;

/**
 * Created by guillermomo on 7/17/15.
 */
public class SkillData
{
    public String _id;
    public String skillTitle;
    public String level;
    public Boolean isUpdated;

    public SkillData(String _id, String skillTitle, String level)
    {
        this._id = _id;
        this.skillTitle = skillTitle;
        this.level = level;
        isUpdated = false;
    }

    public String get_id()
    {
        return _id;
    }

    public void set_id(String _id)
    {
        this._id = _id;
    }

    public String getSkillTitle()
    {
        return skillTitle;
    }

    public void setSkillTitle(String skillTitle)
    {
        this.skillTitle = skillTitle;
    }

    public String getLevel()
    {
        return level;
    }

    public void setLevel(String level)
    {
        this.level = level;
    }

    public Boolean getIsUpdated()
    {
        return isUpdated;
    }

    public void setIsUpdated(Boolean isUpdated)
    {
        this.isUpdated = isUpdated;
    }
}
