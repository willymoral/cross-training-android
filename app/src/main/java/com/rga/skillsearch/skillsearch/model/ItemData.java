package com.rga.skillsearch.skillsearch.model;

/**
 * Created by guillermomo on 7/3/15.
 */
public class ItemData
{
    public String title;
    public String subTitle;

    public ItemData(String title, String subTitle)
    {
        this.title = title;
        this.subTitle = subTitle;
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getSubTitle()
    {
        return subTitle;
    }

    public void setSubTitle(String subTitle)
    {
        this.subTitle = subTitle;
    }

}