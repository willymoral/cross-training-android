package com.rga.skillsearch.skillsearch.fragment;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import com.rga.skillsearch.skillsearch.R;
import com.rga.skillsearch.skillsearch.adapter.MySkillAdapter;
import com.rga.skillsearch.skillsearch.api.ServiceManager;
import com.rga.skillsearch.skillsearch.db.DataManager;
import com.rga.skillsearch.skillsearch.response.LoginResponse;
import com.rga.skillsearch.skillsearch.response.MySkillResponse;
import java.util.ArrayList;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by guillermomo on 10/8/15.
 */

public class MySkillFragment extends Fragment
{
    private RecyclerView recyclerView;
    private MySkillAdapter adapter;
    private ArrayList itemsData = new ArrayList();
    private Context context;
    private ProgressDialog progress;

    public MySkillFragment(Context context)
    {
        this.context = context;
        this.progress = new ProgressDialog(this.context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        final View rootView = inflater.inflate(R.layout.fragment_my_skills, container, false);

        recyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        DataManager dataManager = DataManager.getInstance();
        LoginResponse loginResponse = dataManager.getLoginResponse();

        this.progress.setMessage("Loading...");
        this.progress.show();

        ServiceManager serviceManager = ServiceManager.getInstance();
        String filter = "username="+loginResponse.getUser().username;
        serviceManager.getMySkillApi().getMySkills(filter, new Callback<MySkillResponse>() {
            @Override
            public void success(MySkillResponse mySkillResponse, Response response)
            {
                itemsData = mySkillResponse.getItem();
                adapter.setItemsData(itemsData);
                adapter.notifyDataSetChanged();
                progress.dismiss();
                Log.w("myApp", "SkillApi - OK!");
            }

            @Override
            public void failure(RetrofitError error)
            {
                progress.dismiss();
                Toast.makeText(context, error.toString(), Toast.LENGTH_LONG).show();
                Log.w("myApp", "SkillApi - Error!");
            }
        });

        adapter = new MySkillAdapter(this.context, itemsData);
        recyclerView.setAdapter(adapter);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        return rootView;
    }

    public void elementClicked()
    {
        Log.d("Skill -", "Element  Clicked");
    }
}
