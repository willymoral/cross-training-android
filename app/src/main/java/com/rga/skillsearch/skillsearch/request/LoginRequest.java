package com.rga.skillsearch.skillsearch.request;

/**
 * Created by guillermomo on 6/26/15.
 */

public class LoginRequest
{
    public String username;
    public String password;

    public LoginRequest(String username, String password) {
        this.username = username;
        this.password = password;
    }
}
