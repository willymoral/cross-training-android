package com.rga.skillsearch.skillsearch.api;

import com.rga.skillsearch.skillsearch.BuildConfig;
import com.rga.skillsearch.skillsearch.db.DataManager;

import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.converter.GsonConverter;

/**
 * Created by guillermomo on 7/1/15.
 */
public class ServiceManager
{
    private static ServiceManager ourInstance = new ServiceManager();

    // Service
    private LoginApi loginApi;
    private SkillsApi skillApi;
    private PhoneListApi phoneListApi;
    private MySkillApi mySkillApi;
    private String urlServices;

    public static ServiceManager getInstance()
    {
        return ourInstance;
    }

    private ServiceManager()
    {
        if(BuildConfig.DEBUG)
        {
            urlServices = "http://demo1804042.mockable.io";
        } else
        {
            urlServices = "http://10.200.49.152";
        }

        RestAdapter aRest = new RestAdapter.Builder()
                .setEndpoint(urlServices)
                .setRequestInterceptor(new RequestInterceptor() {
                    @Override
                    public void intercept(RequestFacade request) {

                        DataManager dataManager = DataManager.getInstance();

                        if (dataManager.isUserLoggedIn()) {
                            request.addHeader("Cookie", dataManager.getCookie());
                        }
                    }
                })
                .build();

        loginApi = aRest.create(LoginApi.class);
        skillApi = aRest.create(SkillsApi.class);
        phoneListApi = aRest.create(PhoneListApi.class);
        mySkillApi = aRest.create(MySkillApi.class);

    }

    public LoginApi getLoginApi()
    {
        return loginApi;
    }

    public SkillsApi getSkillApi()
    {
        return  skillApi;
    }

    public PhoneListApi getPhoneListApi()
    {
        return phoneListApi;
    }

    public MySkillApi getMySkillApi()
    {
        return mySkillApi;
    }
}
