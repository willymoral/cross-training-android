package com.rga.skillsearch.skillsearch.api;

import com.rga.skillsearch.skillsearch.response.MySkillResponse;
import com.rga.skillsearch.skillsearch.response.PhoneListResponse;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Query;

/**
 * Created by guillermomo on 7/7/15.
 */
public interface MySkillApi
{
    @GET("/api/user")
    void getPhoneList(@Query("filters[]") String value, Callback<PhoneListResponse> callback);

    @GET("/api/user")
    void getMySkills(@Query(value = "filters[]", encodeValue = false) String value, Callback<MySkillResponse> callback);
}
