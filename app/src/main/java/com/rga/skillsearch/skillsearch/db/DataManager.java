package com.rga.skillsearch.skillsearch.db;

import android.util.Log;

import com.rga.skillsearch.skillsearch.response.LoginResponse;
import com.rga.skillsearch.skillsearch.response.SkillResponse;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by guillermomo on 6/30/15.
 */
public class DataManager
{
    private static DataManager ourInstance = new DataManager();
    private LoginResponse loginResponse = null;
    private SkillResponse allSkillResponse = null;
    private String cookie;
    private Set<SkillResponse.Category> categorys;

    public static DataManager getInstance()
    {
        return ourInstance;
    }

    private DataManager()
    {

    }

    public LoginResponse getLoginResponse()
    {
        return loginResponse;
    }

    public void setLoginResponse(LoginResponse loginResponse)
    {
        this.loginResponse = loginResponse;
    }

    public Boolean isUserLoggedIn()
    {
        if (loginResponse!=null)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public Boolean isAllSkill()
    {
        if (allSkillResponse!=null)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public void setAllSkillResponse(SkillResponse skillResponse)
    {
        this.allSkillResponse = skillResponse;
    }

    public SkillResponse getAllSkillResponse()
    {
        return this.allSkillResponse;
    }

    public String getCookie()
    {
        return cookie;
    }

    public void setCookie(String cookie)
    {
        this.cookie = cookie;
    }

    public void setCategorys(Set<SkillResponse.Category> categorys)
    {
        this.categorys = categorys;
    }

    public ArrayList<SkillResponse.Category> getCategorys()
    {
        return new ArrayList<SkillResponse.Category>(this.categorys);
    }

    public ArrayList<SkillResponse.Data> getSkillsByCategoryId(String categoryId)
    {
        ArrayList<SkillResponse.Data> skillData = new ArrayList<SkillResponse.Data>();

        List<SkillResponse.Data> allSkill = this.allSkillResponse.getData();

        for(SkillResponse.Data skill:allSkill)
        {
            if(skill.category.getIdCategory().equalsIgnoreCase(categoryId))
            {
                skillData.add(skill);
            }
        }

        return skillData;
    }

    public ArrayList<SkillResponse.Category> getCategoryByUser()
    {
        Set<SkillResponse.Category> tmpCategory = new HashSet<SkillResponse.Category>();

        List<LoginResponse.SkillLevels> aLogin = this.loginResponse.user.getSkillLevels();

        for(LoginResponse.SkillLevels loginSkill:aLogin)
        {
            List<SkillResponse.Data> allSkill = this.allSkillResponse.getData();

            for(SkillResponse.Data skill:allSkill)
            {
                if(skill.idSkill.equalsIgnoreCase(loginSkill.idSkill))
                {
                    tmpCategory.add(skill.category);
                }
            }
        }

        return new ArrayList<SkillResponse.Category>(tmpCategory);
    }
}
