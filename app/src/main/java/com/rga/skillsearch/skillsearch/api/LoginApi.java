package com.rga.skillsearch.skillsearch.api;

import com.rga.skillsearch.skillsearch.request.LoginRequest;
import com.rga.skillsearch.skillsearch.response.LoginResponse;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.POST;

/**
 * Created by guillermomo on 6/26/15.
 */

public interface LoginApi
{
    @POST("/api/session")
    void login(@Body LoginRequest userPass, Callback<LoginResponse> cb);
}
