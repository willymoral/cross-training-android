package com.rga.skillsearch.skillsearch.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by guillermomo on 7/7/15.
 */

public class PhoneListResponse
{
    @SerializedName("_id")
    public String idData;
    public String ADkey;
    public String ID;
    public String buildFloor;
    public String department;
    public String desk_phone;
    public String email;
    public String first;
    public String group;
    public String last;
    public String office;
    public String photoURL;
    @SerializedName("title")
    public String titleData;
}
