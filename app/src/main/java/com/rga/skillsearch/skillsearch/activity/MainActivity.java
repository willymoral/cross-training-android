package com.rga.skillsearch.skillsearch.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.rga.skillsearch.skillsearch.R;
import com.rga.skillsearch.skillsearch.api.ServiceManager;
import com.rga.skillsearch.skillsearch.db.DataManager;
import com.rga.skillsearch.skillsearch.request.LoginRequest;
import com.rga.skillsearch.skillsearch.response.LoginResponse;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Header;
import retrofit.client.Response;

public class MainActivity extends Activity
{
    public ProgressDialog progress;

    @Override
    protected void onStart()
    {
        super.onStart();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        progress = new ProgressDialog(this);
        setContentView(R.layout.activity_main);

        EditText usernameText = (EditText) findViewById(R.id.usernameText);
        EditText passwordText = (EditText) findViewById(R.id.passwordText);

        usernameText.setText("guillermomo");
        passwordText.setText("Titas2015#");

        usernameText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                EditText username = (EditText) findViewById(R.id.usernameText);
                username.setError(null);
            }
        });

        passwordText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                EditText password = (EditText) findViewById(R.id.passwordText);
                password.setError(null);
            }
        });

        Button buttonClick = (Button) findViewById(R.id.loginButton);

        buttonClick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText username = (EditText) findViewById(R.id.usernameText);
                EditText password = (EditText) findViewById(R.id.passwordText);

                String usernameString = username.getText().toString();
                String passwordString = password.getText().toString();

                if (usernameString.equals("") || passwordString.equals("")) {
                    if (usernameString.equals("")) {
                        username.setError(getString(R.string.username_error_main));
                    }

                    if (passwordString.equals("")) {
                        password.setError(getString(R.string.password_error_main));
                    }
                } else {

                    progress.setMessage("Checking");
                    progress.show();

                    ServiceManager serviceManager = ServiceManager.getInstance();
                    serviceManager.getLoginApi().login(new LoginRequest(usernameString, passwordString), new Callback<LoginResponse>() {

                        @Override
                        public void success(LoginResponse loginResponse, Response response) {
                            progress.dismiss();
                            DataManager dataManager = DataManager.getInstance();
                            dataManager.setLoginResponse(loginResponse);

                            for (Header header : response.getHeaders()) {
                                String myHeader = header.getName();
                                Log.w("myApp", "Header ->" + myHeader);

                                if (myHeader != null) {
                                    if (myHeader.toLowerCase().equals("set-cookie")) {
                                        String[] cookie = header.getValue().split(";");
                                        String myCookie = cookie[0];
                                        Log.w("myApp", "Cookie ->" + myCookie);
                                        dataManager.setCookie(myCookie);
                                        break;
                                    }
                                }
                            }

                            // Go to next View
                            Intent intent = new Intent(MainActivity.this, MainScreenActivity.class);
                            startActivity(intent);
                        }

                        @Override
                        public void failure(RetrofitError error)
                        {
                            progress.dismiss();
                            Log.w("myApp", "Login - Error!");
                            Toast.makeText(getApplicationContext(),
                                    getString(R.string.login_message_error), Toast.LENGTH_LONG).show();
                        }
                    });
                }
            }
        });
    }
}