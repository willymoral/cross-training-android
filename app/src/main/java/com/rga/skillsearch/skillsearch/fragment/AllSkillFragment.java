package com.rga.skillsearch.skillsearch.fragment;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.rga.skillsearch.skillsearch.R;
import com.rga.skillsearch.skillsearch.adapter.AllCategoryAdapter;
import com.rga.skillsearch.skillsearch.db.DataManager;
import java.util.ArrayList;

/**
 * Created by guillermomo on 10/8/15.
 */

public class AllSkillFragment extends Fragment
{
    private RecyclerView recyclerView;
    private AllCategoryAdapter adapter;
    private ArrayList itemsData = new ArrayList();
    private Context context;
    private ProgressDialog progress;

    public AllSkillFragment(Context context)
    {
        this.context = context;
        this.progress = new ProgressDialog(this.context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        final View rootView = inflater.inflate(R.layout.fragment_category_my_skills, container, false);

        recyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        DataManager dataManager = DataManager.getInstance();

        this.itemsData = dataManager.getCategoryByUser();

        // Set Data
        adapter = new AllCategoryAdapter(this.context, itemsData);
        recyclerView.setAdapter(adapter);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        return rootView;
    }

    public void elementClicked()
    {
        Log.d("Skill -", "Element  Clicked");
    }
}