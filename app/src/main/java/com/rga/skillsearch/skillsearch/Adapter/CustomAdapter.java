package com.rga.skillsearch.skillsearch.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.rga.skillsearch.skillsearch.R;
import com.rga.skillsearch.skillsearch.model.ItemData;

/**
 * Created by guillermomo on 7/3/15.
 */
public class CustomAdapter extends RecyclerView.Adapter<CustomAdapter.ViewHolder>
{
    private ItemData[] itemsData;

    public CustomAdapter(ItemData[] itemsData)
    {
        this.itemsData = itemsData;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public CustomAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType)
    {
        // Create a new view
        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item, null);

        // Create ViewHolder
        ViewHolder viewHolder = new ViewHolder(itemLayoutView);
        return viewHolder;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position)
    {
        viewHolder.mySkillTextView.setText(itemsData[position].getTitle());
    }

    // inner class to hold a reference to each item of RecyclerView
    public static class ViewHolder extends RecyclerView.ViewHolder
    {
        public TextView mySkillTextView;

        public ViewHolder(View itemLayoutView)
        {
            super(itemLayoutView);
            mySkillTextView = (TextView) itemLayoutView.findViewById(R.id.skillTextView);
        }
    }

    // Return the size of your itemsData (invoked by the layout manager)
    @Override
    public int getItemCount()
    {
        return itemsData.length;
    }
}