package com.rga.skillsearch.skillsearch.adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import com.rga.skillsearch.skillsearch.R;
import com.rga.skillsearch.skillsearch.api.ServiceManager;
import com.rga.skillsearch.skillsearch.model.SkillData;
import com.rga.skillsearch.skillsearch.request.UpdateSkillRequest;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by guillermomo on 7/15/15.
 */
public class MySkillAdapter extends RecyclerView.Adapter<MySkillAdapter.ViewHolder>
{

    private List<SkillData> itemsData = Collections.emptyList();
    private List<SkillData> saveItemsData = new ArrayList<SkillData>();
    private Context context;
    AdapterView.OnItemClickListener itemClickListener;
    private ProgressDialog progress;

    public MySkillAdapter(Context context, ArrayList itemsData)
    {
        this.itemsData = itemsData;
        this.context = context;
        this.progress = new ProgressDialog(this.context);
    }

    public MySkillAdapter(ArrayList itemsData)
    {
        this.itemsData = itemsData;
    }

    public void saveSkills()
    {
        Log.d("MySkillAdapter", "Save");

        this.saveItemsData.clear();

        for (int i=0; i < this.itemsData.size(); i++)
        {
            SkillData currentItem = this.itemsData.get(i);

            if (currentItem.isUpdated) {
                this.saveItemsData.add(currentItem);
            }
        }

        if (this.saveItemsData.size() > 0)
        {
            this.progress.setMessage("Updating...");
            this.progress.show();
            this.callSaveSkill();
        }
    }

    void callSaveSkill()
    {
        if(this.saveItemsData.size() > 0)
        {
            SkillData currentItem = this.saveItemsData.get(0);
            ServiceManager serviceManager = ServiceManager.getInstance();

            UpdateSkillRequest updateSkillRequest = new UpdateSkillRequest(currentItem.get_id(), Integer.parseInt(currentItem.getLevel()));

            serviceManager.getSkillApi().updateSkill(updateSkillRequest, new Callback<Response>()
            {
                @Override
                public void success(Response result, Response response) {

                    BufferedReader reader = null;
                    StringBuilder sb = new StringBuilder();
                    try {
                        reader = new BufferedReader(new InputStreamReader(result.getBody().in()));
                        String line;
                        try {
                            while ((line = reader.readLine()) != null) {
                                sb.append(line);
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    String bodyResponse = sb.toString();

                    if (sb.toString().equalsIgnoreCase("skill created")) {
                        Log.d("Update Skill OK -", "Element " + " Body: " + bodyResponse);
                        SkillData currentItem = saveItemsData.get(0);
                        currentItem.setIsUpdated(false);
                        saveItemsData.remove(0);
                        callSaveSkill();
                    } else {
                        progress.dismiss();
                        Log.d("Update Skill Error -", "Element " + " Body: " + bodyResponse);
                    }
                }

                @Override
                public void failure(RetrofitError error)
                {
                    Log.d("Update Skill Error -", "Element " + " clicked.");
                    progress.dismiss();
                    Toast.makeText(context, error.toString(), Toast.LENGTH_LONG).show();
                }
            });
        } else
        {
            // Finish
            progress.dismiss();
        }
    }

    // Create new views (invoked by the layout manager)
    @Override
    public MySkillAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                       int viewType)
    {
        // Create a new view
        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.my_skill_item, null);

        // Create ViewHolder
        ViewHolder viewHolder = new ViewHolder(itemLayoutView);
        return viewHolder;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position)
    {
        SkillData tmp = itemsData.get(position);
        viewHolder.skilltextView.setText(tmp.getSkillTitle());
        viewHolder.seekBar.setProgress(Integer.parseInt(tmp.getLevel()));
        viewHolder.seekBar.setMax(4);
    }

    // inner class to hold a reference to each item of RecyclerView
    public class ViewHolder extends RecyclerView.ViewHolder
    {
        public TextView skilltextView;
        public SeekBar seekBar;

        public ViewHolder(View v)
        {
            super(v);

            seekBar = (SeekBar) v.findViewById(R.id.seekBar);
            skilltextView = (TextView) v.findViewById(R.id.skillTextView);

            v.setOnClickListener(
                    new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d("Skill -", "Element " + getPosition() + " clicked.");
                    //mAdapterCallback.onMethodCallback();
                }
            });

            seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener()
            {
                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser)
                {
                    Log.d("seekBar - Changed", "Element " + getPosition());
                    SkillData currentItem = itemsData.get(getPosition());
                    currentItem.setLevel(String.valueOf(progress));
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar)
                {
                    Log.d("seekBar - Star", "Element " + getPosition());
                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar)
                {
                    SkillData currentItem = itemsData.get(getPosition());
                    currentItem.setIsUpdated(true);
                    Log.d("seekBar - Stop", "Element " + getPosition());
                    Log.d("seekBar - Stop", "__________________________________");
                }
            });
        }
    }

    // Return the size of your itemsData (invoked by the layout manager)
    @Override
    public int getItemCount()
    {
        return itemsData.size();
    }

    public void setItemsData(ArrayList itemsData)
    {
        this.itemsData.clear();
        this.itemsData.addAll(itemsData);
    }
}