package com.rga.skillsearch.skillsearch.response;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by guillermomo on 7/2/15.
 */
public class SkillResponse
{
    public Pager pager;
    public List<Data> data;

    public class Pager
    {
        public String p;
        public String total_pages;
        public String ipp;
        public String page_count;
        public String total_count;
        public String filters;
        // public String sort;      TODO : Research
        public String prev;
        public String current;
        public String next;
    }

    public class Data implements Serializable
    {
        @SerializedName("_id")
        public String idSkill;
        @SerializedName("_version")
        public String versionData;
        public Category category;
        public String title;
        public String updated;
        public String created;
        public String order;
        public String userCreated;
        @SerializedName("_link")
        public String linkData;
    }

    public class Category implements Serializable
    {
        public String getTitle()
        {
            return title;
        }

        public String getIdCategory()
        {
            return IdCategory;
        }

        public String title;
        @SerializedName("_id")
        public String IdCategory;
        @SerializedName("_version")
        public String versionCategory;
        public String updated;
        public String created;

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Category category = (Category) o;

            return !(IdCategory != null ? !IdCategory.equals(category.IdCategory) : category.IdCategory != null);

        }

        @Override
        public int hashCode() {
            return IdCategory != null ? IdCategory.hashCode() : 0;
        }
    }

    public Set<Category> getCategory()
    {
        Set<Category> set = new HashSet<Category>();

        int dataCount = this.data.size();

        for(int i=0; i < dataCount ; i++)
        {
            set.add(this.data.get(i).category);
        }

        return set;
    }

    public List<Data>getData()
    {
        return this.data;
    }
}