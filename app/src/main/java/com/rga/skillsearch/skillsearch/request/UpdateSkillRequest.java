package com.rga.skillsearch.skillsearch.request;

import com.google.gson.annotations.SerializedName;

/**
 * Created by guillermomo on 7/20/15.
 */

public class UpdateSkillRequest
{
    public String id;
    public int level;

    public UpdateSkillRequest(String id, int level)
    {
        this.id = id;
        this.level = level;
    }
}
