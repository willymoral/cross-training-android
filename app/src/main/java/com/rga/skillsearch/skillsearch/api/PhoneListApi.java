package com.rga.skillsearch.skillsearch.api;

import com.rga.skillsearch.skillsearch.response.PhoneListResponse;
import com.rga.skillsearch.skillsearch.response.SkillResponse;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Path;
import retrofit.http.Query;

/**
 * Created by guillermomo on 7/7/15.
 */
public interface PhoneListApi
{
    @GET("/api/phonelist/{email}")
    void getPhoneList(@Path("email") String value, Callback<PhoneListResponse> callback);
}
