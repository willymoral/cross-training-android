package com.rga.skillsearch.skillsearch.adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;
import com.rga.skillsearch.skillsearch.R;
import com.rga.skillsearch.skillsearch.activity.SkillsDetailsActivity;
import com.rga.skillsearch.skillsearch.response.SkillResponse;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.ViewHolder>
{
    private List<SkillResponse.Category> itemsData = Collections.emptyList();
    private Context context;
    AdapterView.OnItemClickListener itemClickListener;
    private ProgressDialog progress;

    public CategoryAdapter(Context context, ArrayList itemsData)
    {
        this.itemsData = itemsData;
        this.context = context;
        this.progress = new ProgressDialog(this.context);
    }

    public CategoryAdapter(ArrayList itemsData)
    {
        this.itemsData = itemsData;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public CategoryAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                        int viewType)
    {
        // Create a new view
        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.skill_item, null);

        // Create ViewHolder
        ViewHolder viewHolder = new ViewHolder(itemLayoutView);
        return viewHolder;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position)
    {
        SkillResponse.Category tmp = itemsData.get(position);
        viewHolder.categoryTextView.setText(tmp.getTitle());
    }

    // inner class to hold a reference to each item of RecyclerView
    public class ViewHolder extends RecyclerView.ViewHolder
    {
        public TextView categoryTextView;

        public ViewHolder(View v)
        {
            super(v);
            categoryTextView = (TextView) v.findViewById(R.id.skillTextView);

            v.setOnClickListener(
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            SkillResponse.Category category = itemsData.get(getPosition());
                            Log.d("Category:", category.getTitle() + " | Position:" + getPosition());

                            Intent intent = new Intent(context, SkillsDetailsActivity.class);
                            intent.putExtra("Category", category);
                            context.startActivity(intent);
                        }
                    });
        }
    }

    // Return the size of your itemsData (invoked by the layout manager)
    @Override
    public int getItemCount()
    {
        return itemsData.size();
    }

    public void setItemsData(ArrayList itemsData)
    {
        this.itemsData.clear();
        this.itemsData.addAll(itemsData);
    }
}