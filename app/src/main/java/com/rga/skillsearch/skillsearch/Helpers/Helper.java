package com.rga.skillsearch.skillsearch.helpers;

import android.app.ProgressDialog;

/**
 * Created by guillermomo on 7/24/15.
 */
public class Helper {
    private static Helper ourInstance = new Helper();

    private ProgressDialog progressDialog;

    public static Helper getInstance() {
        return ourInstance;
    }

    private Helper()
    {

    }
}
