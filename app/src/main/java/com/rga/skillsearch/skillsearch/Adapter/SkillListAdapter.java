package com.rga.skillsearch.skillsearch.adapter;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.rga.skillsearch.skillsearch.R;
import com.rga.skillsearch.skillsearch.model.ItemData;

/**
 * Created by guillermomo on 7/7/15.
 */

public class SkillListAdapter extends RecyclerView.Adapter<SkillListAdapter.ViewHolder>
{
    private ItemData[] itemsData;

    public SkillListAdapter(ItemData[] itemsData)
    {
        this.itemsData = itemsData;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public SkillListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                       int viewType)
    {
        // Create a new view
        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.skill_item, null);

        // Create ViewHolder
        ViewHolder viewHolder = new ViewHolder(itemLayoutView);
        return viewHolder;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position)
    {
        viewHolder.txtViewTitle.setText(itemsData[position].getTitle());
    }

    // inner class to hold a reference to each item of RecyclerView
    public static class ViewHolder extends RecyclerView.ViewHolder
    {
        public TextView txtViewTitle;

        public ViewHolder(View v)
        {
            super(v);

            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d("Skill -", "Element " + getPosition() + " clicked.");

                }
            });

            txtViewTitle = (TextView) v.findViewById(R.id.titleTextView);
        }
    }

    // Return the size of your itemsData (invoked by the layout manager)
    @Override
    public int getItemCount()
    {
        return itemsData.length;
    }
}
